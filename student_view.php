<?php
include "inc/header.php";
include  "lib/Student.php";
?>

    <script>
        $(document).ready(function(){
            $("form").submit(function () {
                var roll = true;
                $(":radio").each(function () {
                    name = $(this).attr('name');
                    if(roll && !$(':radio[name="' +name +'"]:checked').length){
                        alert(name + "Roll missing !");
                        $('.alert').show();
                        roll = false;
                    }
                });
                return roll;
            });
        });
    </script>

<?php
error_reporting(0);
$st = new Student();
$dt = $_GET['dt'];
//    $curr_date = now();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $attend = $_POST['attend'];
    $updateData = $st->updateData($dt, $attend);
}
?>
<?php
if(isset($updateData)){
    echo $updateData;
}

?>
    <div class='alert alert-danger' style="display: none;"><strong>Erro !</strong> Student Roll missing !</div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>
                <a class="btn btn-success" href="add.php" >Add Student</a>
                <a class="btn btn-info pull-right" href="date_view.php">Back</a>
            </h2>
        </div>


        <div class="panel-body">
            <div class="well text-center" style="font-size: 20px;" >
                <strong>Date:</strong> <?php echo date('Y-m-d'); ?>
            </div>
            <form action="" method="post">
                <table class="table table-striped">

                    <tr>
                        <th width="25%">Serial</th>
                        <th width="25%">Student Name</th>
                        <th width="25%">Student Roll</th>
                        <th width="25%">Attendance</th>
                    </tr>

                    <?php
                    $st = new Student();

                    $getAllData = $st->getAllData($dt);
                    if($getAllData){
                        $i = 0;
                        while($result = $getAllData->fetch_assoc()){
                            $i++;

                            ?>

                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $result['name']; ?></td>
                                <td><?php echo $result['roll']; ?></td>
                                <td>
                                    <input type="radio" name="attend[<?php echo $result['roll']; ?>]" value="present" <?php if($result['attend'] == 'present'){echo "checked";} ?>>P
                                    <input type="radio" name="attend[<?php echo $result['roll']; ?>]" value="absent" <?php if($result['attend'] == 'absent'){echo "checked";} ?>>A
                                </td>
                            </tr>


                        <?php } }    ?>

                    <tr>
                        <td colspan="4">
                            <input type="submit" class="btn btn-primary" name="submit" value="Update">
                        </td>
                    </tr>

                </table>
            </form>
        </div>
    </div>



<?php
include "inc/footer.php";
?>