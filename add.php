<?php
    include "inc/header.php";
    include  "lib/Student.php";
?>

<?php
    $st = new Student();
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $name = $_POST['name'];
        $roll = $_POST['roll'];
        $insertData = $st->insertData($name, $roll);
    }
?>
<?php
    if(isset($insertData)){
       // echo "<div class='alert alert-danger'><strong>Error !</strong> Field must not be empty</div>";
        echo $insertData;
    }
?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>
                <a class="btn btn-success" href="add.php">Student Add</a>
                <a class="btn btn-primary pull-right" href="index.php">Back</a>
            </h2>
        </div>

        <div class="panel-body">
            <form action="" method="post">
                
                <div class="form-group">
                    <label for="name">Student Name</label>
                    <input type="text" name="name"  id="name" class="form-control" placeholder="Enter name..." >
                </div>
                
                <div class="form-group">
                    <label for="roll">Student Roll</label>
                    <input type="text" name="roll" id="roll" class="form-control" placeholder="Enter Roll.." >
                </div>

                <div class="form-group">
                    <input type="submit" name="submit" class="btn btn-primary" value="Add Student">
                </div>
                
            </form>
        </div>

    </div>








<?php
    include "inc/footer.php";
?>
