<?php
    include "inc/header.php";
    include  "lib/Student.php";
?>

<script>
    $(document).ready(function(){
        $("form").submit(function () {
            var roll = true;
            $(":radio").each(function () {
                name = $(this).attr('name');
                if(roll && !$(':radio[name="' +name +'"]:checked').length){
                    //alert(name + "Roll missing !");
                    $('.alert').show();
                    roll = false;
                }
            });
            return roll;
        });
    });
</script>

<?php
    error_reporting(0);
    $st = new Student();
//    $curr_date = now();
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $attend = $_POST['attend'];
        $insertData = $st->insertAttData($attend);
    }
?>
<?php
    if(isset($insertData)){
        echo $insertData;
    }

?>
<div class='alert alert-danger' style="display: none;"><strong>Erro !</strong> Student Roll missing !</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2>
            <a class="btn btn-success" href="add.php" >Add Student</a>
            <a class="btn btn-info pull-right" href="date_view.php">View All</a>
        </h2>
    </div>


    <div class="panel-body">
        <div class="well text-center" style="font-size: 20px;" >
            <strong>Date:</strong> <?php echo date('Y-m-d'); ?>
        </div>
        <form action="" method="post">
            <table class="table table-striped">

                <tr>
                    <th width="25%">Serial</th>
                    <th width="25%">Student</th>
                    <th width="25%">Roll</th>
                    <th width="25%">Attendance</th>
                </tr>

                <?php
                    $getStudent = $st->getStudent();
                    if($getStudent){
                        $i = 0;
                        while($result = $getStudent->fetch_assoc()){
                            $i++;

                ?>

                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $result['name']; ?></td>
                    <td><?php echo $result['roll']; ?></td>
                    <td>
                        <input type="radio" name="attend[<?php echo $result['roll']; ?>]" value="present">P
                        <input type="radio" name="attend[<?php echo $result['roll']; ?>]" value="absent">A
                    </td>
                </tr>


                <?php } }    ?>

                <tr>
                    <td colspan="4">
                        <input type="submit" class="btn btn-primary" name="submit" value="Submit">
                    </td>
                </tr>

            </table>
        </form>
    </div>
</div>



<?php
include "inc/footer.php";
?>