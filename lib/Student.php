<?php
    $filepath = realpath(dirname(__FILE__));
    include_once "$filepath"."/Database.php";
?>

<?php

    class Student{
        private $db;

        public function  __construct()
        {
            $this->db = new Database();
        }

        public function getStudent(){
            $query = "select * from tbl_student";
            $result = $this->db->select($query);
            return $result;
        }

        public function  insertData($name, $roll){
            $name = mysqli_real_escape_string($this->db->link, $name);
            $roll = mysqli_real_escape_string($this->db->link, $roll);
            if(empty($name) || empty($roll)){
                $msg = "<div class='alert alert-danger'><strong>Error !</strong> Field must not be empty</div>";
                return $msg;
            }
            else{
                $st_query = "insert into tbl_student(name, roll) values('$name', '$roll')";
                $insertData = $this->db->insert($st_query);

                $att_query = "insert into tbl_attendance(roll) values('$roll')";
                $insertData = $this->db->insert($att_query);

                if($insertData){
                    $msg = "<div class='alert alert-success'><strong>Succes :</strong> Student data inserted successfully</div>";
                    return $msg;
                }
                else{
                    $msg = "<div class='alert alert-danger'><strong>Error !</strong> Student data inserted successfully</div>";
                    return $msg;
                }
            }
        }


        //insert student attedance value into tbl_attendance table
        public function insertAttData($attend){
            $currDate = date('Y-m-d');
            $query = "select distinct att_time from tbl_attendance";
            $result = $this->db->select($query);
            while ($getDate = $result->fetch_assoc()){
                $dbDate = $getDate['att_time'];
                if($currDate == $dbDate){
                    $msg = "<div class='alert alert-danger'><strong>Erro !</strong>Attendance already taken.</div>";
                    return $msg;
                }
            }

            foreach ($attend as $att_key => $att_value){
                if($att_value == 'present'){
                    $query = "insert into tbl_attendance(roll, attend, att_time) values('$att_key', 'present', now())";
                    $data_insert = $this->db->insert($query);
                }
                elseif($att_value == 'absent'){
                    $query = "insert into tbl_attendance(roll, attend, att_time) values('$att_key', 'absent', now())";
                    $data_insert = $this->db->insert($query);
                }
            }

            if($data_insert){
                $msg = "<div class='alert alert-success'><strong>Success !</strong>Attendance data inserted successfully</div>";
                return $msg;
            }
            else{
                $msg = "<div class='alert alert-danger'><strong>Erro !</strong>Attendance data not inserted !</div>";
                return $msg;
            }

        }


        //get unique date for view date_view page
        public function getDate(){
            $query = "select distinct att_time from tbl_attendance";
            $result = $this->db->select($query);
            return $result;
        }


        //fetch all data to view student view page .
        public function  getAllData($dt){
            $query = "select tbl_student.name, tbl_attendance.*
                        from tbl_student
                        inner join tbl_attendance
                        on tbl_student.roll = tbl_attendance.roll
                        where att_time='$dt'                    
            ";
            $result = $this->db->select($query);
            return $result;
        }

        public function updateData($dt, $attend){
            foreach ($attend as $att_key => $att_value){
                if($att_value == 'present'){
                    $update = "update tbl_attendance
                                set
                                attend = 'present'
                                where roll = '".$att_key."'and att_time = '".$dt."'";

                    $update = $this->db->update($update);

                }
                elseif($att_value == 'absent'){
                    $update = "update tbl_attendance
                                set
                                attend = 'absent'
                                where roll = '".$att_key."'and att_time = '".$dt."'";

                    $update = $this->db->update($update);
                }
            }
            if($update){
                $msg = "<div class='alert alert-success'><strong>Success !</strong>Attendance data updated successfully</div>";
                return $msg;
            }
            else{
                $msg = "<div class='alert alert-danger'><strong>Erro !</strong>Attendance data not updated !</div>";
                return $msg;
            }
        }



    }

?>