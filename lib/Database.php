<?php
    $filepath = realpath(dirname(__FILE__));
    include_once "$filepath".'/../config/config.php';
?>

<?php

class Database
{
    public $host = DB_HOST;
    public $user = DB_USER;
    public $pass = DB_PASS;
    public $dbname = DB_NAME;

    public $link;
    public $error;

    public function __construct()
    {
        $this->connectDB();
    }

    private function connectDB()
    {
        $this->link = new mysqli($this->host, $this->user, $this->pass, $this->dbname);
        if(!$this->link){
            $this->error = "Connection fail.".$this->link->connect_errno;
            return false;

        }
       /* else{
            echo "connection created.";
        }*/
    }

    //select or read data from database
    public function select($query){
        $result  = $this->link->query($query) or die($this->link->error.__LINE__);
        if($result->num_rows > 0){
            return $result;
        }
        else{
            return false;
        }
    }

    //insert data into database
    public function insert($query){
        $insertRow = $this->link->query($query) or die($this->link->error.__LINE__);
        if($insertRow){
            return $insertRow;
        }
        else{
            return false;
        }
    }

    //update data in datebase
    public function update($query){
        $updateData = $this->link->query($query) or die($this->link->error.__LINE__);
        if(updateData){
            return $updateData;
        }
        else{
            return $updateData;
        }
    }


}

?>