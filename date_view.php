<?php
    include "inc/header.php";
    include "lib/Student.php";
?>

<div class="panel panel-default">

    <div class="panel panel-heading">
        <h2>
            <a class="btn btn-primary" href="add.php">Add Student</a>
            <a class="btn btn-info pull-right" href="index.php">Take Attendance</a>
        </h2>
    </div>

    <div class="panel-body">
        <div class="well text-center" style="font-size: 20px;">
            <strong>Date:</strong><?php echo date('Y-m-d'); ?>
        </div>
    </div>

    <form action="" method="post">
        <table class="table table-responsive">
            <tr>
                <th width="30%">Serial</th>
                <th width="50%">Attendance Date</th>
                <th width="20%">Action</th>
            </tr>
            
            <?php 
                $st = new Student();
                $getDate = $st->getDate();
                if($getDate){
                    $i = 0;
                    while($result = $getDate->fetch_assoc()){
                        $i++;
                  
            ?>
            
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $result['att_time']; ?></td>
                <td>
                    <a class="btn btn-primary" href="student_view.php?dt=<?php echo $result['att_time']; ?>">View</a>
                </td>
            </tr>
            
            <?php } } ?>
            
        </table>
    </form>



</div>











<?php
    include "inc/footer.php";

?>
